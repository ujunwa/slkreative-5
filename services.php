<?php include('header.php'); ?>

<!-- Document Title
	============================================= -->
	<title>What We Do | SL Kreativez</title>


<?php include('nav.php'); ?>

		<!-- Page Title
		============================================= -->
		<section id="page-title" style="background-image: url('images/bg1.jpg'); background-size: cover; padding: 80px 0;  ">

			<div class="container clearfix">
				<h1 style="color: white; font-weight: bolder;">Services</h1>
				<span style="color: white;font-weight: bolder;">Our Interest cuts across different fields</span>
				<ol class="breadcrumb"">
					<li><a href="index.php" style="color: white;font-weight: bolder;">Home</a></li>
					<li><a href="#" style="color: white;font-weight: bolder;">Pages</a></li>
					<li class="active" style="color: white;font-weight: bolder;">What We Do</li>
				</ol>
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">


				<div class="container clearfix">

					<div class="col_one_third">
						<div class="feature-box media-box">
							<div class="fbox-media">
								<img src="images/article.jpg" alt="SL Kreativez">
							</div>
							<div class="fbox-desc">
								<h3>Articles and Research Documentary</h3>
								<p>Because we care about getting the Nigerian mind informed, we have made it a point of duty to curate research on fields and topics that will be beneficial to our community. These topics can be used for academic purposes and for learning. <br>We encourage our community to suggest topics they would have us research on. To leave your suggestions, please write us at info@seekerslocus.com.</p>
							</div>
						</div>
					</div>

					<div class="col_one_third ">
						<div class="feature-box media-box">
							<div class="fbox-media">
								<img src="images/movie.jpg" alt="SL Kreativez">
							</div>
							<div class="fbox-desc">
								<h3>Nollywood and Stage Production Reviews</h3>
								<p>This is your number one stop for professional reviews and critique of Nollywood films and stage arts. As part of our creative quest, we are on our way to making professional reviews a norm in the Nigeria artistic field. <br>We are not on this quest to fault-find the works under critique. Neither do we do so to spite any artist, we value and hold them in the highest regards. This is purely a professional pursuit <!--that is meant to keep them aiming for the next bar raise. We owe no one apologies if their works belittle the expected professional standards. What we do owe everyone is our desire to see better works that can withstand the test of time. We have a purpose and duty-bound to make Nollywood and the stage craft the best in every way.<br>Finally not every film will find their way here, at least at this initial stage but you can rely on us to deliver --></p>
							</div>
						</div>
					</div>

					<div class="col_one_third col_last">
						<div class="feature-box media-box">
							<div class="fbox-media">
								<img src="images/conference.jpg" alt="SL Kreativez">
							</div>
							<div class="fbox-desc">
								<h3>Conferences/Seminars and Art Workshops</h3>
								<p>Learn! Learn! Learn! We have to keep learning, networking, developing till we become like diamonds. As part of our social goals, we will engage our community in creativity inclined experiences. </p><p><a href="projects.php" data-lightbox="image" class="button button-3d noleftmargin">Read More</a></p>
							</div>
						</div>
					</div>
	
					<div class="clearfix"></div>
					
					<div class="col_one_third">
						<div class="feature-box media-box">
							<div class="fbox-media">
								<img src="images/interview.jpg" alt="SL Kreativez">
							</div>
							<div class="fbox-desc">
								<h3>Exclusive Industry Interviews</h3>
								<p>Who wouldn't love to celebrate those who makes our heart dance with words well crafted? We would! We bring you in-depth interviews of writers, performers, art outfits, students making a difference and all the other juicy stuffs on the world of creative enterprise.</p>
							</div>
						</div>
					</div>

					<div class="col_one_third">
						<div class="feature-box media-box">
							<div class="fbox-media">
								<img src="images/drama.jpg" alt="SL Kreativez">
							</div>
							<div class="fbox-desc">
								<h3>Mini-drama Productions</h3>
								<p>You can watch videos on different segments of what we do. This is in the Loading!</p><p><a href="images/5060.jpg" data-lightbox="image" class="button button-3d noleftmargin">Watch Mini Drama</a></p>
							</div>
						</div>
					</div>

					<div class="col_one_third col_last">
						<div class="feature-box media-box">
							<div class="fbox-media">
								<img src="images/writing.jpg" alt="SL Kreativez">
							</div>
							<div class="fbox-desc">
								<h3>Creative Writing Projects</h3>
								<p>Yes, remember we're a for-profit social enterprise. This is where we channel our social goals. </p><p><a href="projects.php" data-lightbox="image" class="button button-3d noleftmargin">Read More</a></p></p>
							</div>
						</div>
					</div>

					<div class="clearfix"></div>

					<div class="col_one_third">
						<div class="feature-box media-box">
							<div class="fbox-media">
								<img src="images/art.jpg" alt="SL Kreativez">
							</div>
							<div class="fbox-desc">
								<h3>African Literature Reviews</h3>
								<p>We want to have an archive of literature reviews that can help students, professionals and literature lovers. Have you recently read a book and you want to hear what we have to say? Chat us or send us a mail via info@seekerslocus.com</p>
							</div>
						</div>
					</div>

					<div class="col_one_third">
						<div class="feature-box media-box">
							<div class="fbox-media">
								<img src="images/charity.jpg" alt="SL Kreativez">
							</div>
							<div class="fbox-desc">
								<h3>Literacy/charity Works</h3>
								<p>Literacy is the bedrock of a forward thinking nation. We are open for partnership and volunteering on any project that will give back to the society. <br>It's our little way of saying â€œThank you for supporting our dreams". Reach us for opportunities to make impact through info@seekerslocus.com</p>
							</div>
						</div>
					</div>

					<div class="col_one_third col_last">
						<div class="feature-box media-box">
							<div class="fbox-media">
								<img src="images/story.jpg" alt="SL Kreativez">
							</div>
							<div class="fbox-desc">
								<h3>Storytelling</h3>
								<p>With the best of sultry, soulful and creatively crafted fiction stories, we promise to keep you informed, entertained and educated. Our storytelling cuts across diverse forms of fiction. From folklore, to didactic, to epic, to satire and pastoral, you won't get bored. We will add more to our shelve as we advance. </p>
							</div>
						</div>
					</div>
				
					<div class="clearfix"></div>

				</div>

				


				<div class="promo promo-dark promo-flat promo-full footer-stick"  style="background-color:#0087BD;">
					<div class="container clearfix text-center">
						<h3 style="font-size: 18px;">Call us today at <span>08012345678</span> or Email us at <span>info@seekerslocus.com</span></h3>
					</div>
				</div>

			</div>

		</section><!-- #content end -->

		<!-- Footer

		============================================= -->
<?php include('footer.php'); ?>